#!/bin/bash

set -e

# Define paths to input and output files
INPUT_DIRECTORY="debian/tests/images/"
EXPECTED_OUTPUT_FILE="debian/tests/output/expected_output.txt"

# Check if the input and output files exist
if [ ! -d "$INPUT_DIRECTORY" ] || [ ! -f "$EXPECTED_OUTPUT_FILE" ]; then
    echo "Input or output file not found!"
    exit 1
fi

# Execute the command and capture the output
exiflooter_output=$(exiflooter -i "$INPUT_DIRECTORY")

# Compare the exiflooter output with the content of the file
if diff -q <(echo "$exiflooter_output" | sort) "$EXPECTED_OUTPUT_FILE" > /dev/null; then
    echo "Output matches the content of the file!"
else
    echo "Output does not match the expected content."
    exit 1
fi
